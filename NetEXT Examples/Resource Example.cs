﻿using System;
using SFML.Window;
using SFML.Graphics;
using SFML.Audio;
using NetEXT.Resources;

namespace NetEXT.Examples
{
    public class Resource_Example
    {
        #region Functions
        public void Run()
        {
            var cache = CacheFactory.CreateMultiResourceCache<string>();
            Image img = new Image(529, 100, new Color(130, 70, 0));
            var texturehandle1 = cache.Aquire<Texture>("texture1", img);
            var texturehandle2 = cache.Aquire<Texture>("texture2", ".\\image.jpg");
            var soundhandle = cache.Aquire<SoundBuffer>("sound", ".\\click.wav");
            var fonthandle = cache.Aquire<Font>("font", ".\\sansation.ttf");
            Sprite sprite1 = new Sprite(texturehandle1);
            Sprite sprite2 = new Sprite(texturehandle2);
            Sound sound = new Sound(soundhandle);
            Text instructions = new Text("Press return to play sound, escape to quit", fonthandle, 14);
            sprite2.Position += new Vector2f(0, texturehandle1.Resource.Size.Y);
            RenderWindow window = new RenderWindow(new VideoMode(800, 600), "NetEXT Resource Example");
            window.SetVerticalSyncEnabled(true);
            window.KeyPressed += (sender, e) => { if (e.Code == Keyboard.Key.Return) sound.Play(); else if (e.Code == Keyboard.Key.Escape) window.Close(); };
            window.Closed += (sender, e) => { window.Close(); };
            while (window.IsOpen())
            {
                window.DispatchEvents();
                window.Clear();
                window.Draw(sprite1);
                window.Draw(sprite2);
                window.Draw(instructions);
                window.Display();
            }
        }
        #endregion
    }
}
