﻿using System;
using NetEXT.Graphics;

namespace NetEXT.Animation
{
    /// <summary>Animates an object's color smoothly over time.</summary>
    /// <typeparam name="T">Type of object to animate</typeparam>
    public class ColorAnimation<T> : IAnimation<T>
    {
        #region Variables
        private ColorGradient _gradient = null;
        #endregion

        #region Properties
        /// <summary>Gets or sets the <see cref="NetEXT.Graphics.ColorGradient"/> that is used animate objects.</summary>
        private ColorGradient ColorGradient
        {
            get
            {
                return _gradient;
            }
            set
            {
                _gradient = value;
            }
        }
        #endregion

        #region Constructors/Destructors
        /// <summary>Constructs a new ColorAnimation using the specified gradient.</summary>
        /// <param name="ColorGradient">Gradient that is used to animate objects</param>
        public ColorAnimation(ColorGradient ColorGradient)
        {
            _gradient = ColorGradient;
        }
        #endregion

        #region Functions
        public void Animate(AnimatedObject<T> AnimatedObject, float Progress)
        {
            AnimatedObject.Color = _gradient.GetColor(Progress);
        }
        #endregion
    }
}
