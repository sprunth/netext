﻿using System;

namespace NetEXT.Animation
{
    /// <summary>Animates an object's transparency smoothly over time.</summary>
    /// <typeparam name="T">Type of object to animate</typeparam>
    public class FadeAnimation<T> : IAnimation<T>
    {
        #region Variables
        private float _fadeinratio = 0;
        private float _fadeoutratio = 0;
        #endregion

        #region Properties
        /// <summary>Gets or sets the part of time during which the object is faded in. Must be in the interval [0, 1].</summary>
        public float FadeInRatio
        {
            get
            {
                return _fadeinratio;
            }
            set
            {
                _fadeinratio = value;
            }
        }
        /// <summary>Gets or sets the part of time during which the object is faded out. Must be in the interval [0, 1-FadeInRatio].</summary>
        public float FadeOutRatio
        {
            get
            {
                return _fadeoutratio;
            }
            set
            {
                _fadeoutratio = value;
            }
        }
        #endregion

        #region Constructors/Destructors
        /// <summary>Constructs a new FadeAnimation with the specified fade ratios.</summary>
        /// <param name="FadeInRatio">The part of time during which the object is faded in. Must be in the interval [0, 1]</param>
        /// <param name="FadeOutRatio">The part of time during which the object is faded out. Must be in the interval [0, 1-FadeInRatio]</param>
        public FadeAnimation(float FadeInRatio, float FadeOutRatio)
        {
            _fadeinratio = FadeInRatio;
            _fadeoutratio = FadeOutRatio;
        }
        #endregion

        #region Functions
        public void Animate(AnimatedObject<T> AnimatedObject, float Progress)
        {
            if (Progress < _fadeinratio)
            {
                AnimatedObject.Color = new SFML.Graphics.Color(AnimatedObject.Color.R, AnimatedObject.Color.G, AnimatedObject.Color.B, (byte)(256f * Progress / _fadeinratio));
            }
            else if (Progress > 1f - _fadeoutratio)
            {
                AnimatedObject.Color = new SFML.Graphics.Color(AnimatedObject.Color.R, AnimatedObject.Color.G, AnimatedObject.Color.B, (byte)(256f * (1f - Progress) / _fadeoutratio));
            }
        }
        #endregion
    }
}
