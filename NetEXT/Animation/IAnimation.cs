﻿using System;

namespace NetEXT.Animation
{
    /// <summary>Interface on which custom animations can be built.</summary>
    /// <typeparam name="T">Type of object to animate</typeparam>
    public interface IAnimation<T>
    {
        /// <summary>Called when an object needs to be animated.</summary>
        /// <param name="AnimatedObject">Object to apply the animation</param>
        /// <param name="Progress">Number in [0, 1] determining the animation state</param>
        void Animate(AnimatedObject<T> AnimatedObject, float Progress);
    }
}
