﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;

namespace NetEXT.Graphics
{
    /// <summary>This class can represent color gradients and calculate the interpolated color at a given point.</summary>
    /// <remarks> A color gradient consists of a list of positions (float values in [0, 1]) and corresponding colors. At each position, a color is sampled exactly; between two positions, a color is interpolated linearly.</remarks>
    public class ColorGradient
    {
        #region Variables
        private Dictionary<float, Color> _colors = new Dictionary<float, Color>();
        private List<float> _keys = new List<float>();
        #endregion

        #region Properties
        /// <summary>Inserts or retrives a color from the gradient.</summary>
        /// <remarks>A map-like syntax allows you to specify the color at a certain position. Warning: At least both positions 0.f and 1.f must be set before a gradient is valid.</remarks>
        /// <param name="Position">Position in the gradient</param>
        public Color this[float Position]
        {
            get
            {
                return _colors[Position];
            }
            set
            {
                if (_keys.Contains(Position))
                {
                    _colors[Position] = value;
                }
                else
                {
                    _colors.Add(Position, value);
                    _keys.Add(Position);
                    _keys.Sort();
                }
            }
        }
        #endregion

        #region Functions
        /// <summary>Blends the colors FirstColor and SecondColor, according to the given Interpolation.</summary>
        /// <param name="FirstColor">First color to blend</param>
        /// <param name="SecondColor">Second color to blend</param>
        /// <param name="Interpolation">Specifies how much of every color is taken. If it has value 0, the first color is returned; with value 1, the second color is returned. Every value within [0,1] leads to a mixture. Other values are not allowed.</param>
        public static Color BlendColors(Color FirstColor, Color SecondColor, float Interpolation)
        {
            float firstpart = 1f - Interpolation;
            return new Color(
                (byte)((firstpart * (float)FirstColor.R) + (Interpolation * (float)SecondColor.R)),
                (byte)((firstpart * (float)FirstColor.G) + (Interpolation * (float)SecondColor.G)),
                (byte)((firstpart * (float)FirstColor.B) + (Interpolation * (float)SecondColor.B)),
                (byte)((firstpart * (float)FirstColor.A) + (Interpolation * (float)SecondColor.A)));
        }
        /// <summary>Interpolates a color in the gradient.</summary>
        /// <param name="Position">Number in [0, 1] that specifies a position in the gradient. When you pass 0 (1), the color at the very beginning (end) is returned.</param>
        public Color GetColor(float Position)
        {
            float nextcolor = 0;
            float prevcolor = 1;
            for (int i = 0; i < _keys.Count; i++)
            {
                if (_keys[i] == Position) return _colors[_keys[i]];
                else if (_keys[i] > Position)
                {
                    nextcolor = _keys[i];
                    if (i > 0) prevcolor = _keys[i - 1];
                    else prevcolor = _keys[_keys.Count - 1];
                    break;
                }
            }
            float interpolation = (Position - prevcolor) / (nextcolor - prevcolor);
            return BlendColors(_colors[prevcolor], _colors[nextcolor], interpolation);
        }
        #endregion
    }
}
