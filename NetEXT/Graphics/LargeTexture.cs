﻿using System;
using System.Collections.Generic;
using System.IO;
using SFML.Window;
using SFML.Graphics;
using SFML.System;

namespace NetEXT.Graphics
{
    /// <summary>Class for textures which are too big for <see cref="SFML.Graphics.Texture"/>.</summary>
    /// <remarks><see cref="SFML.Graphics.Texture"/> cannot handle textures of which the size exceeds the hardware-given limit. Instead, you can use this class, which has a very similar interface to <see cref="SFML.Graphics.Texture"/>. Internally, the class splits the texture into smaller parts which are unproblematic for OpenGL. To display the texture on the screen, you can use <see cref="NetEXT.Graphics.LargeSprite"/>.</remarks>
    public class LargeTexture : IDisposable
    {
        #region Variables
        private Texture[] _texturelist = null;
        private Vector2u[] _positionlist = null;
        private bool _issmoothed = false;
        private Vector2u _totalsize = new Vector2u(0, 0);
        private bool _isdisposed = false;
        #endregion

        #region Properties
        /// <summary>Gets or sets if the texture should be smoothed.</summary>
        public bool Smooth
        {
            get
            {
                return _issmoothed;
            }
            set
            {
                _issmoothed = value;
                for (int i = 0; i < _texturelist.Length; i++)
                {
                    _texturelist[i].Smooth = value;
                }
            }
        }
        /// <summary>Gets the size of the texture.</summary>
        public Vector2u Size
        {
            get
            {
                return _totalsize;
            }
        }
        internal Texture[] TextureList
        {
            get
            {
                return _texturelist;
            }
        }
        internal Vector2u[] PositionList
        {
            get
            {
                return _positionlist;
            }
        }
        #endregion

        #region Constructors/Destructors
        /// <summary>Loads the texture from a file.</summary>
        /// <param name="Filename">Path of the file to load</param>
        public LargeTexture(string Filename)
        {
            Image img = new Image(Filename);
            Create(img);
            img.Dispose();
            img = null;
        }
        /// <summary>Loads the texture from an image.</summary>
        /// <param name="Image">Image from which to load the texture</param>
        public LargeTexture(Image Image)
        {
            Create(Image);
        }
        /// <summary>Loads the texture from a stream.</summary>
        /// <param name="Stream">Stream from which to load the texture</param>
        public LargeTexture(Stream Stream)
        {
            Image img = new Image(Stream);
            Create(img);
            img.Dispose();
            img = null;
        }
        /// <summary>Creates a texture with the specified Width and Height.</summary>
        /// <param name="Width">Width of the new texture</param>
        /// <param name="Height">Height of the new texture</param>
        public LargeTexture(uint Width, uint Height)
        {
            Image img = new Image(Width, Height);
            Create(img);
            img.Dispose();
            img = null;
        }
        /// <summary>Creates a texture with the specified Width and Height from the Pixel array.</summary>
        /// <param name="Width">Tidth of the new texture</param>
        /// <param name="Height">Height of the new texture</param>
        /// <param name="Pixels">RGBA pixel array from which to load the texture</param>
        public LargeTexture(uint Width, uint Height, byte[] Pixels)
        {
            Image img = new Image(Width, Height, Pixels);
            Create(img);
            img.Dispose();
            img = null;
        }
        /// <summary>Creates a texture from the specified 2D array of colors.</summary>
        /// <param name="Pixels">2D color array from which to load the texture</param>
        public LargeTexture(Color[,] Pixels)
        {
            Image img = new Image(Pixels);
            Create(img);
            img.Dispose();
            img = null;
        }
        private void Create(Image CurrentImage)
        {
            List<Texture> newtextlist = new List<Texture>();
            List<Vector2u> newposlist = new List<Vector2u>();
            uint maxsize = Texture.MaximumSize;
            for (uint y = 0; y < CurrentImage.Size.Y; y += maxsize)
            {
                for (uint x = 0; x < CurrentImage.Size.X; x += maxsize)
                {
                    Texture newtexture = new Texture(CurrentImage, new IntRect((int)x, (int)y, (int)Math.Min(maxsize, CurrentImage.Size.X - x), (int)Math.Min(maxsize, CurrentImage.Size.Y - y)));
                    newtexture.Repeated = false;
                    newtexture.Smooth = _issmoothed;
                    newtextlist.Add(newtexture);
                    newposlist.Add(new Vector2u(x, y));
                }
            }
            _texturelist = newtextlist.ToArray();
            _positionlist = newposlist.ToArray();
            _totalsize = CurrentImage.Size;
        }
        internal LargeTexture(LargeRenderTexture CurrentRenderTexture)
        {
            _texturelist = new Texture[CurrentRenderTexture.RenderTextureList.Length];
            for (int i = 0; i < CurrentRenderTexture.RenderTextureList.Length; i++)
            {
                _texturelist[i] = CurrentRenderTexture.RenderTextureList[i].Texture;
            }
            _totalsize = CurrentRenderTexture.Size;
            _positionlist = CurrentRenderTexture.PositionList;
        }
        /// <summary>Disposes this texture and any other owned resources.</summary>
        public void Dispose()
        {
            if (!_isdisposed)
            {
                _isdisposed = true;
                for (int i = 0; i < _texturelist.Length; i++)
                {
                    _texturelist[i].Dispose();
                    _texturelist[i] = null;
                }
                _texturelist = null;
            }
        }
        #endregion

        #region Functions
        /// <summary>Updates the texture from the Pixel array.</summary>
        /// <param name="Pixels">RGBA pixel array from which to update the texture</param>
        public void Update(byte[] Pixels)
        {
            if (_texturelist.Length > 1)
            {
                for (int i = 0; i < _texturelist.Length; i++)
                {
                    byte[] subpixels = new byte[_texturelist[i].Size.X * _texturelist[i].Size.Y * 4];
                    for (int y = 0; y < _texturelist[i].Size.Y; y++)
                    {
                        Array.Copy(Pixels, (_positionlist[i].X * 4) + (((y + _positionlist[i].Y) * _totalsize.X * 4)), subpixels, _texturelist[i].Size.X * y * 4, _texturelist[i].Size.X * 4);
                    }
                    _texturelist[i].Update(subpixels, _texturelist[i].Size.X, _texturelist[i].Size.Y, 0, 0);
                }
            }
            else if (_texturelist.Length == 1)
            {
                _texturelist[0].Update(Pixels, _totalsize.X, _totalsize.Y, 0, 0);
            }
        }
        // TODO Add more Update overloads
        #endregion
    }
}
