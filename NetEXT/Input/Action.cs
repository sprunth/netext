﻿using System;
using SFML.Window;

namespace NetEXT.Input
{
    /// <summary>Class for dynamic actions that are connected with SFML events</summary>
    /// <remarks>Use this class to build complex constellations of different events (key strokes, pressed mouse buttons, etc...) and associate it with an <see cref="NetEXT.Input.ActionMap"/> instance.</remarks> 
    public class Action
    {
        #region Variables
        private ActionNode _operation = null;
        #endregion

        #region Constructors/Destructors
        /// <summary>Construct key action.</summary>
        /// <remarks>Creates an action that is in effect when a key is manipulated. The second parameter specifies whether KeyPressed events, KeyReleased events or Keyboard.IsKeyPressed(...) act as action source.</remarks>
        public Action(Keyboard.Key Key, ActionType Action = ActionType.Hold)
        {
            switch (Action)
            {
                case ActionType.Hold:
                    _operation = new RealtimeKeyLeaf(Key);
                    break;
                case ActionType.PressOnce:
                case ActionType.ReleaseOnce:
                    _operation = new EventKeyLeaf(Key, Action == ActionType.PressOnce);
                    break;
            }
        }
        /// <summary>Construct mouse button action.</summary>
        /// <remarks>Creates an action that is in effect when a mouse button is manipulated. The second parameter specifies whether MouseButtonPressed events, MouseButtonReleased events or Mouse.IsButtonPressed(...) act as action source.</remarks>
        public Action(Mouse.Button MouseButton, ActionType Action = ActionType.Hold)
        {
            switch (Action)
            {
                case ActionType.Hold:
                    _operation = new RealtimeMouseLeaf(MouseButton);
                    break;
                case ActionType.PressOnce:
                case ActionType.ReleaseOnce:
                    _operation = new EventMouseLeaf(MouseButton, Action == ActionType.PressOnce);
                    break;
            }
        }
        /// <summary>Construct joystick button action.</summary>
        /// <remarks>Creates an action that is in effect when a joystick button is manipulated. The second parameter specifies whether JoyButtonPressed events, JoyButtonReleased events or Joystick.IsButtonPressed(...) act as action source.</remarks>
        public Action(JoystickButton JoystickState, ActionType Action = ActionType.Hold)
        {
            switch (Action)
            {
                case ActionType.Hold:
                    _operation = new RealtimeJoystickButtonLeaf(JoystickState);
                    break;
                case ActionType.PressOnce:
                case ActionType.ReleaseOnce:
                    _operation = new EventJoystickButtonLeaf(JoystickState, Action == ActionType.PressOnce);
                    break;
            }
        }
        /// <summary>Construct joystick axis action.</summary>
        /// <remarks>Creates an action that is in effect when the absolute value of the joystick axis position exceeds a threshold. The source of the action is Joystick.GetAxisPosition(...) and not JoystickMoved events. This implies that the action will also be active if the axis remains unchanged in a position above the threshold.</remarks>
        public Action(JoystickAxis JoystickState)
        {
            _operation = new RealtimeJoystickAxisLeaf(JoystickState);
        }
        /// <summary>Creates a custom action that operates on events.</summary>
        /// <param name="Filter">Callback that is called for every event (which is passed as a parameter). It shall return true when the passed event makes the action active.</param>
        public Action(Func<Event, bool> Filter)
        {
            _operation = new CustomEventLeaf(Filter);
        }
        /// <summary>Creates a custom action that operates on realtime input.</summary>
        /// <param name="Filter">Callback that is called exactly once per frame, independent of any events. It shall return true when a certain realtime input state should make the action active.</param>
        public Action(Func<bool> Filter)
        {
            _operation = new CustomRealtimeLeaf(Filter);
        }
        /// <summary>Construct SFML event action.</summary>
        /// <remarks>Creates an action that is in effect when a SFML event of the specified type is fired.</remarks>
        public Action(EventType EventType)
        {
            _operation = new MiscEventLeaf(EventType);
        }
        private Action(ActionNode Operation)
        {
            _operation = Operation;
        }
        #endregion

        #region Functions
        internal bool IsActive(EventBuffer Buffer)
        {
            return _operation.IsActionActive(Buffer);
        }
        internal bool IsActive(EventBuffer Buffer, ActionResult Result)
        {
            return _operation.IsActionActive(Buffer, Result);
        }
        #endregion

        #region Operators
        public static Action operator |(Action LHS, Action RHS)
        {
            return new Action(new OrNode(LHS._operation, RHS._operation));
        }
        public static Action operator &(Action LHS, Action RHS)
        {
            return new Action(new AndNode(LHS._operation, RHS._operation));
        }
        public static Action operator !(Action Action)
        {
            return new Action(new NotNode(Action._operation));
        }
        #endregion
    }
}
