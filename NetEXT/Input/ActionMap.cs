﻿using System;
using System.Collections.Generic;
using SFML.Window;

namespace NetEXT.Input
{
    /// <summary>Class that associates identifiers with dynamic actions.</summary>
    /// <remarks>You can use this class to map identifiers like strings or enumerators to a specific combination of SFML events and real time input states. After the initialization, IsActive(...) provides an easy way to check whether a specified identifier is associated with an active action. Furthermore, it is possible to map the actions to callbacks, which can be achieved with InvokeCallbacks(...).</remarks>
    public class ActionMap<T>
    {
        #region Variables
        private Dictionary<T, Action> _actionmap = new Dictionary<T, Action>();
        private EventBuffer _eventbuffer = new EventBuffer();
        #endregion

        #region Properties
        /// <summary>Gets or sets an action that is associated with an identifier.</summary>
        /// <param name="ID">Identifier that will be used to identify an action</param>
        public Action this[T ID]
        {
            get
            {
                if (_actionmap.ContainsKey(ID)) return _actionmap[ID];
                else return null;
            }
            set
            {
                if (_actionmap.ContainsKey(ID)) _actionmap[ID] = value;
                else _actionmap.Add(ID, value);
            }
        }
        #endregion

        #region Functions
        /// <summary>Initializes an <see cref="NetEXT.Input.EventSystem"/> that is designed to work with action maps.</summary>
        /// <returns>EventSystem with an event ID handler setup to work with actions.</returns>
        public static EventSystem<T, ActionContext<T>> CreateCallbackSystem()
        {
            return new EventSystem<T, ActionContext<T>>() { GetEventIDHandler = (evt) => { return evt.ActionID; } };
        }
        /// <summary>Clears old events and polls the window for new ones.</summary>
        /// <remarks>When you invoke this method, you should not call Window.DispatchEvents() in the same frame, since Update(...) already does that.</remarks>
        /// <param name="Window">SFML window from which events are polled</param>
        public void Update(Window Window)
        {
            _eventbuffer.ClearEvents();
            _eventbuffer.PollEvents(Window);
        }
        /// <summary>Feeds the action map with a SFML event, that can be used during the current frame.</summary>
        /// <remarks>When you use the Update(...) method, you needn't invoke PushEvent(...). This method exists for more flexibility: You can push user-defined events, and you can do something else with the events before calling PushEvent(...).</remarks>
        /// <param name="Event">Event that will be added for the current frame</param>
        public void PushEvent(Event Event)
        {
            _eventbuffer.PushEvent(Event);
        }
        /// <summary>Removes the events that have been temporarily stored.</summary>
        /// <remarks>You only need this function in combination with PushEvent(...), if you want to feed the action map manually with events. Otherwise, you can just call Update(...).</remarks>
        public void ClearEvents()
        {
            _eventbuffer.ClearEvents();
        }
        /// <summary>Remove the action associated with the specified ID.</summary>
        /// <param name="ID">Identifier that will be used to identify an action</param>
        public void RemoveAction(T ID)
        {
            _actionmap.Remove(ID);
        }
        /// <summary>Returns whether the action associated with the specified identifier is currently in effect.</summary>
        /// <remarks>To be in effect, the boolean operation of the assigned action must yield true. Note that in contrast to registered callbacks, IsActive(...) doesn't take into account the situation where multiple events of the same type occur in a single frame.</remarks>
        /// <param name="ID">Identifier that will be used to identify an action</param>
        public bool IsActive(T ID)
        {
            if (!_actionmap.ContainsKey(ID)) return false;
            return _actionmap[ID].IsActive(_eventbuffer);
        }
        /// <summary>Forwards active actions to a callback system.</summary>
        /// <remarks>For every action that is currently active, the action ID is passed to the system, where all listener functions associated with the ID are invoked.</remarks>
        /// <param name="System">Callback system of type EventSystem<T, ActionContext<T>></param>
        /// <param name="Window">Window reference which is stored in the ActionContext passed to the callbacks - can be null</param>
        public void InvokeCallbacks(EventSystem<T, ActionContext<T>> System, Window Window)
        {
            foreach (var action in _actionmap)
            {
                ActionResult result = new ActionResult();
                if (!action.Value.IsActive(_eventbuffer, result)) continue;
                foreach (Event evt in result.EventContainer)
                {
                    System.TriggerEvent(new ActionContext<T>(Window, evt, action.Key));
                }
                if (result.RealtimeTriggers > 0) System.TriggerEvent(new ActionContext<T>(Window, null, action.Key));
            }
        }
        #endregion
    }
}
