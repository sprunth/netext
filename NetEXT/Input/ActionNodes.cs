﻿using System;
using SFML.Window;

namespace NetEXT.Input
{
    internal abstract class ActionNode
    {
        public abstract bool IsActionActive(EventBuffer Buffer);
        public abstract bool IsActionActive(EventBuffer Buffer, ActionResult Result);
    }
    internal abstract class RealtimeNode : ActionNode
    {
        public override bool IsActionActive(EventBuffer Buffer)
        {
            return Buffer.RealtimeInputEnabled && IsRealtimeActive();
        }
        public override bool IsActionActive(EventBuffer Buffer, ActionResult Result)
        {
            if (IsActionActive(Buffer))
            {
                Result.RealtimeTriggers += 1;
                return true;
            }
            else return false;
        }
        public abstract bool IsRealtimeActive();
    }
    internal abstract class EventNode : ActionNode
    {
        protected Event _event;
        public override bool IsActionActive(EventBuffer Buffer)
        {
            return Buffer.ContainsEvent(this);
        }
        public override bool IsActionActive(EventBuffer Buffer, ActionResult Result)
        {
            return Buffer.FilterEvents(this, Result.EventContainer);
        }
        public abstract bool IsEventActive(Event Event);
    }
    internal class RealtimeKeyLeaf : RealtimeNode
    {
        private Keyboard.Key _key;
        public RealtimeKeyLeaf(Keyboard.Key Key)
        {
            _key = Key;
        }
        public override bool IsRealtimeActive()
        {
            return Keyboard.IsKeyPressed(_key);
        }
    }
    internal class EventKeyLeaf : EventNode
    {
        public EventKeyLeaf(Keyboard.Key Key, bool Pressed)
        {
            _event.Type = Pressed ? EventType.KeyPressed : EventType.KeyReleased;
            _event.Key.Code = Key;
        }
        public override bool IsEventActive(Event Event)
        {
            return Event.Type == _event.Type && Event.Key.Code == _event.Key.Code;
        }
    }
    internal class RealtimeMouseLeaf : RealtimeNode
    {
        private Mouse.Button _button;
        public RealtimeMouseLeaf(Mouse.Button MouseButton)
        {
            _button = MouseButton;
        }
        public override bool IsRealtimeActive()
        {
            return Mouse.IsButtonPressed(_button);
        }
    }
    internal class EventMouseLeaf : EventNode
    {
        public EventMouseLeaf(Mouse.Button MouseButton, bool Pressed)
        {
            _event.Type = Pressed ? EventType.MouseButtonPressed : EventType.MouseButtonReleased;
            _event.MouseButton.Button = MouseButton;
        }
        public override bool IsEventActive(Event Event)
        {
            return Event.Type == _event.Type && Event.MouseButton.Button == _event.MouseButton.Button;
        }
    }
    internal class RealtimeJoystickButtonLeaf : RealtimeNode
    {
        private JoystickButton _joystickbutton;
        public RealtimeJoystickButtonLeaf(JoystickButton JoystickButton)
        {
            _joystickbutton = JoystickButton;
        }
        public override bool IsRealtimeActive()
        {
            return SFML.Window.Joystick.IsButtonPressed(_joystickbutton.JoystickID, _joystickbutton.Button);
        }
    }
    internal class EventJoystickButtonLeaf : EventNode
    {
        public EventJoystickButtonLeaf(JoystickButton JoystickState, bool Pressed)
        {
            _event.Type = Pressed ? EventType.JoystickButtonPressed : EventType.JoystickButtonReleased;
            _event.JoystickButton.JoystickId = JoystickState.JoystickID;
            _event.JoystickButton.Button = JoystickState.Button;
        }
        public override bool IsEventActive(Event Event)
        {
            return Event.Type == _event.Type && Event.JoystickButton.Button == _event.JoystickButton.Button;
        }
    }
    internal class RealtimeJoystickAxisLeaf : RealtimeNode
    {
        private JoystickAxis _joystickaxis;
        public RealtimeJoystickAxisLeaf(JoystickAxis JoystickAxis)
        {
            _joystickaxis = JoystickAxis;
        }
        public override bool IsRealtimeActive()
        {
            float axispos = SFML.Window.Joystick.GetAxisPosition(_joystickaxis.JoystickID, _joystickaxis.Axis);
            return _joystickaxis.Above && axispos > _joystickaxis.Threshold || !_joystickaxis.Above && axispos < _joystickaxis.Threshold;
        }
    }
    internal class CustomEventLeaf : EventNode
    {
        private Func<Event, bool> _filter = null;
        public CustomEventLeaf(Func<Event, bool> Filter)
        {
            _filter = Filter;
        }
        public override bool IsEventActive(Event Event)
        {
            return _filter(Event);
        }
    }
    internal class CustomRealtimeLeaf : RealtimeNode
    {
        private Func<bool> _filter = null;
        public CustomRealtimeLeaf(Func<bool> Filter)
        {
            _filter = Filter;
        }
        public override bool IsRealtimeActive()
        {
            return _filter();
        }
    }
    internal class MiscEventLeaf : EventNode
    {
        public MiscEventLeaf(EventType EventType)
        {
            _event.Type = EventType;
        }
        public override bool IsEventActive(Event Event)
        {
            return Event.Type == _event.Type;
        }
    }
    internal class OrNode : ActionNode
    {
        private ActionNode _lhs;
        private ActionNode _rhs;
        public OrNode(ActionNode LHS, ActionNode RHS)
        {
            _lhs = LHS;
            _rhs = RHS;
        }
        public override bool IsActionActive(EventBuffer Buffer)
        {
            return _lhs.IsActionActive(Buffer) || _rhs.IsActionActive(Buffer);
        }
        public override bool IsActionActive(EventBuffer Buffer, ActionResult Result)
        {
            bool lhsres = _lhs.IsActionActive(Buffer, Result);
            bool rhsres = _rhs.IsActionActive(Buffer, Result);
            return lhsres || rhsres;
        }
    }
    internal class AndNode : ActionNode
    {
        private ActionNode _lhs;
        private ActionNode _rhs;
        public AndNode(ActionNode LHS, ActionNode RHS)
        {
            _lhs = LHS;
            _rhs = RHS;
        }
        public override bool IsActionActive(EventBuffer Buffer)
        {
            return _lhs.IsActionActive(Buffer) && _rhs.IsActionActive(Buffer);
        }
        public override bool IsActionActive(EventBuffer Buffer, ActionResult Result)
        {
            ActionResult tmpresult = new ActionResult();
            if (_lhs.IsActionActive(Buffer, tmpresult) && _rhs.IsActionActive(Buffer, tmpresult))
            {
                Result.EventContainer.AddRange(tmpresult.EventContainer);
                Result.RealtimeTriggers += tmpresult.RealtimeTriggers;
                return true;
            }
            else return false;
        }
    }
    internal class NotNode : ActionNode
    {
        private ActionNode _action;
        public NotNode(ActionNode Action)
        {
            _action = Action;
        }
        public override bool IsActionActive(EventBuffer Buffer)
        {
            return !_action.IsActionActive(Buffer);
        }
        public override bool IsActionActive(EventBuffer Buffer, ActionResult Result)
        {
            ActionResult tmpresult = new ActionResult();
            if (!_action.IsActionActive(Buffer, tmpresult))
            {
                Result.EventContainer.AddRange(tmpresult.EventContainer);
                Result.RealtimeTriggers += tmpresult.RealtimeTriggers;
                return true;
            }
            else return false;
        }
    }
}
