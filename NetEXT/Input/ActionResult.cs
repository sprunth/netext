﻿using System;
using System.Collections.Generic;
using SFML.Window;

namespace NetEXT.Input
{
    internal class ActionResult
    {
        public List<Event> EventContainer = new List<Event>();
        public int RealtimeTriggers = 0;
    }
}
