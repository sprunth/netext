﻿using System;

namespace NetEXT.Input
{
    public enum ActionType
    {
        /// <summary>Repeated input (e.g. a key that is held down).</summary>
        Hold,
        /// <summary>Press events that occur only once (e.g. key pressed).</summary>
        PressOnce,
        /// <summary>Release events that occur only once (e.g. key released).</summary>
        ReleaseOnce
    }
}
