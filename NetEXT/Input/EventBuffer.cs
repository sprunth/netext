﻿using System;
using System.Linq;
using System.Collections.Generic;
using SFML.Window;

namespace NetEXT.Input
{
    internal class EventBuffer
    {
        #region Variables
        private bool _realtimeenabled = true;
        private List<Event> _events = new List<Event>();
        #endregion

        #region Properties
        public bool RealtimeInputEnabled
        {
            get
            {
                return _realtimeenabled;
            }
        }
        #endregion

        #region Functions
        public void PollEvents(Window Window)
        {
            AttachEvents(Window);
            Window.DispatchEvents();
            DetachEvents(Window);
        }
        #region Event Handling
        // Only reason we are handling events like this is because we have no access to Window.pollEvent(..) - No thanks to Laurent xD
        private void AttachEvents(Window Window)
        {
            Window.Closed += WindowClosed;
            Window.GainedFocus += WindowGainedFocus;
            Window.JoystickButtonPressed += WindowJoystickButtonPressed;
            Window.JoystickButtonReleased += WindowJoystickButtonReleased;
            Window.JoystickMoved += WindowJoystickMoved;
            Window.JoystickConnected += WindowJoystickConnected;
            Window.JoystickDisconnected += WindowJoystickDisconnected;
            Window.KeyPressed += WindowKeyPressed;
            Window.KeyReleased += WindowKeyReleased;
            Window.LostFocus += WindowLostFocus;
            Window.MouseButtonPressed += WindowMouseButtonPressed;
            Window.MouseButtonReleased += WindowMouseButtonReleased;
            Window.MouseEntered += WindowMouseEntered;
            Window.MouseLeft += WindowMouseLeft;
            Window.MouseMoved += WindowMouseMoved;
            Window.MouseWheelMoved += WindowMouseWheelMoved;
            Window.Resized += WindowResized;
            Window.TextEntered += WindowTextEntered;
        }
        private void DetachEvents(Window Window)
        {
            Window.Closed -= WindowClosed;
            Window.GainedFocus -= WindowGainedFocus;
            Window.JoystickButtonPressed -= WindowJoystickButtonPressed;
            Window.JoystickButtonReleased -= WindowJoystickButtonReleased;
            Window.JoystickMoved -= WindowJoystickMoved;
            Window.JoystickConnected -= WindowJoystickConnected;
            Window.JoystickDisconnected -= WindowJoystickDisconnected;
            Window.KeyPressed -= WindowKeyPressed;
            Window.KeyReleased -= WindowKeyReleased;
            Window.LostFocus -= WindowLostFocus;
            Window.MouseButtonPressed -= WindowMouseButtonPressed;
            Window.MouseButtonReleased -= WindowMouseButtonReleased;
            Window.MouseEntered -= WindowMouseEntered;
            Window.MouseLeft -= WindowMouseLeft;
            Window.MouseMoved -= WindowMouseMoved;
            Window.MouseWheelMoved -= WindowMouseWheelMoved;
            Window.Resized -= WindowResized;
            Window.TextEntered -= WindowTextEntered;
        }
        private void WindowClosed(object sender, EventArgs e)
        {
            PushEvent(new Event() { Type = EventType.Closed });
        }
        private void WindowGainedFocus(object sender, EventArgs e)
        {
            PushEvent(new Event() { Type = EventType.GainedFocus });
        }
        private void WindowJoystickButtonPressed(object sender, JoystickButtonEventArgs e)
        {
            PushEvent(new Event() { Type = EventType.JoystickButtonPressed, JoystickButton = new JoystickButtonEvent() { Button = e.Button, JoystickId = e.JoystickId } });
        }
        private void WindowJoystickButtonReleased(object sender, JoystickButtonEventArgs e)
        {
            PushEvent(new Event() { Type = EventType.JoystickButtonReleased, JoystickButton = new JoystickButtonEvent() { Button = e.Button, JoystickId = e.JoystickId } });
        }
        private void WindowJoystickMoved(object sender, JoystickMoveEventArgs e)
        {
            PushEvent(new Event() { Type = EventType.JoystickMoved, JoystickMove = new JoystickMoveEvent() { Axis = e.Axis, JoystickId = e.JoystickId, Position = e.Position } });
        }
        private void WindowJoystickConnected(object sender, JoystickConnectEventArgs e)
        {
            PushEvent(new Event() { Type = EventType.JoystickConnected, JoystickConnect = new JoystickConnectEvent() { JoystickId = e.JoystickId } });
        }
        private void WindowJoystickDisconnected(object sender, JoystickConnectEventArgs e)
        {
            PushEvent(new Event() { Type = EventType.JoystickDisconnected, JoystickConnect = new JoystickConnectEvent() { JoystickId = e.JoystickId } });
        }
        private void WindowKeyPressed(object sender, KeyEventArgs e)
        {
            PushEvent(new Event() { Type = EventType.KeyPressed, Key = new KeyEvent() { Alt = e.Alt ? 1 : 0, Code = e.Code, Control = e.Control ? 1 : 0, Shift = e.Shift ? 1 : 0, System = e.System ? 1 : 0 } });
        }
        private void WindowKeyReleased(object sender, KeyEventArgs e)
        {
            PushEvent(new Event() { Type = EventType.KeyReleased, Key = new KeyEvent() { Alt = e.Alt ? 1 : 0, Code = e.Code, Control = e.Control ? 1 : 0, Shift = e.Shift ? 1 : 0, System = e.System ? 1 : 0 } });
        }
        private void WindowLostFocus(object sender, EventArgs e)
        {
            PushEvent(new Event() { Type = EventType.LostFocus });
        }
        private void WindowMouseButtonPressed(object sender, MouseButtonEventArgs e)
        {
            PushEvent(new Event() { Type = EventType.MouseButtonPressed, MouseButton = new MouseButtonEvent() { Button = e.Button, X = e.X, Y = e.Y } });
        }
        private void WindowMouseButtonReleased(object sender, MouseButtonEventArgs e)
        {
            PushEvent(new Event() { Type = EventType.MouseButtonReleased, MouseButton = new MouseButtonEvent() { Button = e.Button, X = e.X, Y = e.Y } });
        }
        private void WindowMouseEntered(object sender, EventArgs e)
        {
            PushEvent(new Event() { Type = EventType.MouseEntered });
        }
        private void WindowMouseLeft(object sender, EventArgs e)
        {
            PushEvent(new Event() { Type = EventType.MouseLeft });
        }
        private void WindowMouseMoved(object sender, MouseMoveEventArgs e)
        {
            PushEvent(new Event() { Type = EventType.MouseMoved, MouseMove = new MouseMoveEvent() { X = e.X, Y = e.Y } });
        }
        private void WindowMouseWheelMoved(object sender, MouseWheelEventArgs e)
        {
            PushEvent(new Event() { Type = EventType.MouseWheelMoved, MouseWheel = new MouseWheelEvent() { Delta = e.Delta, X = e.X, Y = e.Y } });
        }
        private void WindowResized(object sender, SizeEventArgs e)
        {
            PushEvent(new Event() { Type = EventType.Resized, Size = new SizeEvent() { Width = e.Width, Height = e.Height } });
        }
        private void WindowTextEntered(object sender, TextEventArgs e)
        {
            PushEvent(new Event() { Type = EventType.TextEntered, Text = new TextEvent() { Unicode = (uint)Char.ConvertToUtf32(e.Unicode, 0) } });
        }
        #endregion
        public void PushEvent(Event Event)
        {
            if (Event.Type == EventType.GainedFocus) _realtimeenabled = true;
            else if (Event.Type == EventType.LostFocus) _realtimeenabled = false;
            _events.Add(Event);
        }
        public void ClearEvents()
        {
            _events.Clear();
        }
        public bool ContainsEvent(EventNode FilterNode)
        {
            List<Event> unused = new List<Event>();
            return FilterEvents(FilterNode, unused);
        }
        public bool FilterEvents(EventNode FilterNode, List<Event> Events)
        {
            int oldsize = Events.Count;
            Events.AddRange(from evt in _events where FilterNode.IsEventActive(evt) select evt);
            return oldsize != Events.Count;
        }
        #endregion
    }
}
