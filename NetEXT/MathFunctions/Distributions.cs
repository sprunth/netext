﻿using System;
using SFML.Window;
using SFML.System;
using NetEXT.TimeFunctions;
using NetEXT.Vectors;

namespace NetEXT.MathFunctions
{
    public static class Distributions
    {
        /// <summary>Uniform random distribution in an int interval.</summary>
        public static Distribution<int> Uniform(int Min, int Max)
        {
            return new Func<int>(() =>
            {
                return RandomGenerator.Random(Min, Max);
            });
        }
        /// <summary>Uniform random distribution in an uint interval.</summary>
        public static Distribution<uint> Uniform(uint Min, uint Max)
        {
            return new Func<uint>(() =>
            {
                return RandomGenerator.Random(Min, Max);
            });
        }
        /// <summary>Uniform random distribution in a float interval.</summary>
        public static Distribution<float> Uniform(float Min, float Max)
        {
            return new Func<float>(() =>
            {
                return RandomGenerator.Random(Min, Max);
            });
        }
        /// <summary>Uniform random distribution in a time interval.</summary>
        public static Distribution<Time> Uniform(Time Min, Time Max)
        {
            return new Func<Time>(() =>
            {
                return Time.FromSeconds(RandomGenerator.Random((float)Min.AsSeconds(), (float)Max.AsSeconds()));
            });
        }
        /// <summary>Uniform random distribution in a rectangle.</summary>
        public static Distribution<Vector2f> Rect(Vector2f Center, Vector2f HalfSize)
        {
            return new Func<Vector2f>(() =>
            {
                return new Vector2f(RandomGenerator.RandomDev(Center.X, HalfSize.X), RandomGenerator.RandomDev(Center.Y, HalfSize.Y));
            });
        }
        /// <summary>Uniform random distribution in a circle.</summary>
        public static Distribution<Vector2f> Circle(Vector2f Center, float Radius)
        {
            return new Func<Vector2f>(() =>
            {
                Vector2f radiusvector = new PolarVector(Radius * (float)Trigonometry.SqrRoot(RandomGenerator.Random(0f, 1f)), RandomGenerator.Random(0f, 360f));
                return Center + radiusvector;
            });
        }
        /// <summary>Vector rotation with a random angle.</summary>
        public static Distribution<Vector2f> Deflect(Vector2f Direction, float MaxRotation)
        {
            return new Func<Vector2f>(() =>
            {
                return Vectors.Vector2Algebra.Rotate(Direction, RandomGenerator.RandomDev(0f, MaxRotation));
            });
        }
    }
}
