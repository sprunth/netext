﻿using System;

namespace NetEXT.MathFunctions
{
    public static class RandomGenerator
    {
        #region Variables
        private static Random _random = new Random(Guid.NewGuid().GetHashCode());
        #endregion

        #region Functions
        /// <summary>Sets the seed of the random number generator.</summary>
        /// <remarks>etting the seed manually is useful when you want to reproduce a given sequence of random numbers. Without calling this function, the seed is different at each program startup.</remarks>
        public static void SetRandomSeed(int Seed)
        {
            _random = new Random(Seed);
        }
        /// <summary>Returns an int random number in the interval [Min, Max].</summary>
        public static int Random(int Min, int Max)
        {
            return _random.Next(Min, Max);
        }
        /// <summary>Returns an int random number in the interval [Middle-Deviation, Middle+Deviation].</summary>
        public static int RandomDev(int Middle, int Deviation)
        {
            return Random(Middle - Deviation, Middle + Deviation);
        }
        /// <summary>Returns an uint random number in the interval [Min, Max].</summary>
        public static uint Random(uint Min, uint Max)
        {
            return (uint)_random.Next((int)Min, (int)Max);
        }
        /// <summary>Returns an uint random number in the interval [Middle-Deviation, Middle+Deviation].</summary>
        public static uint RandomDev(uint Middle, uint Deviation)
        {
            return Random(Middle - Deviation, Middle + Deviation);
        }
        /// <summary>Returns a float random number in the interval [Min, Max]</summary>
        public static float Random(float Min, float Max)
        {
            return (float)_random.Next((int)(Min * 1000f), (int)(Max * 1000f)) / 1000f;
        }
        /// <summary>Returns a float random number in the interval [Middle-Deviation, Middle+Deviation]</summary>
        public static float RandomDev(float Middle, float Deviation)
        {
            return Random(Middle - Deviation, Middle + Deviation);
        }
        #endregion
    }
}
