﻿using System;
using NetEXT.Concurrent;

namespace NetEXT.Networking
{
    public abstract class ReliableTransport : Lockable
    {
        #region Events
        public event Action<ReliableTransport> DataRecieved;
        public event Action<ReliableTransport> Connected;
        public event Action<ReliableTransport> Disconnected;
        #endregion

        #region Properties
        public abstract int SendingBufferLength { get; }
        public abstract int ReceivingBufferLength { get; }
        public abstract bool IsConnected { get; }
        #endregion

        #region Functions
        public abstract void ClearBuffers();
        protected void OnDataRecieved()
        {
            if (DataRecieved != null) DataRecieved(this);
        }
        protected void OnConnected()
        {
            if (Connected != null) Connected(this);
        }
        protected void OnDisconnected()
        {
            if (Disconnected != null) Disconnected(this);
        }
        public abstract void SendData(byte[] Data);
        public abstract void Connect();
        public abstract void Disconnect();
        public abstract void Shutdown();
        public abstract byte[] PeakReceivingBuffer(int Length);
        public abstract byte[] HandleReceivingBuffer(int Length);
        #endregion
    }
}
