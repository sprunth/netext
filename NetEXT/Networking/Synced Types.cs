﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using SFML.Window;
using SFML.Graphics;
using SFML.System;
using NetEXT.Utility;

namespace NetEXT.Networking
{
    public abstract class SyncedType<T> : SyncedTypeBase
    {
        #region Variables
        protected T _value;
        #endregion

        #region Properties
        public T Value { get { return _value; } set { _value = value; OnNotifyChanged(); } }
        #endregion

        #region Constructors
        public SyncedType(SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Owner, SynchronizationType)
        { _value = Activator.CreateInstance<T>(); }
        public SyncedType(T Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Owner, SynchronizationType)
        { _value = Value; }
        #endregion

        #region Operators
        public static implicit operator T(SyncedType<T> SyncedObject)
        {
            return SyncedObject.Value;
        }
        #endregion
    }
    public class SyncedBool : SyncedType<bool>
    {
        #region Constructors
        public SyncedBool(SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(false, Owner, SynchronizationType) { }
        public SyncedBool(bool Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType) { }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            Message.WriteBool(_value);
        }
        protected override void OnDeserialize(Message Message)
        {
            _value = Message.ReadBool();
        }
        #endregion
    }
    public class SyncedShort : SyncedType<short>
    {
        #region Constructors
        public SyncedShort(SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(0, Owner, SynchronizationType) { }
        public SyncedShort(short Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType) { }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            Message.WriteShort(_value);
        }
        protected override void OnDeserialize(Message Message)
        {
            _value = Message.ReadShort();
        }
        #endregion
    }
    public class SyncedUShort : SyncedType<ushort>
    {
        #region Constructors
        public SyncedUShort(SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(0, Owner, SynchronizationType) { }
        public SyncedUShort(ushort Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType) { }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            Message.WriteUShort(_value);
        }
        protected override void OnDeserialize(Message Message)
        {
            _value = Message.ReadUShort();
        }
        #endregion
    }
    public class SyncedInt : SyncedType<int>
    {
        #region Constructors
        public SyncedInt(SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(0, Owner, SynchronizationType) { }
        public SyncedInt(int Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType) { }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            Message.WriteInt(_value);
        }
        protected override void OnDeserialize(Message Message)
        {
            _value = Message.ReadInt();
        }
        #endregion
    }
    public class SyncedUInt : SyncedType<uint>
    {
        #region Constructors
        public SyncedUInt(SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(0, Owner, SynchronizationType) { }
        public SyncedUInt(uint Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType) { }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            Message.WriteUInt(_value);
        }
        protected override void OnDeserialize(Message Message)
        {
            _value = Message.ReadUInt();
        }
        #endregion
    }
    public class SyncedLong : SyncedType<long>
    {
        #region Constructors
        public SyncedLong(SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(0, Owner, SynchronizationType) { }
        public SyncedLong(long Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType) { }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            Message.WriteLong(_value);
        }
        protected override void OnDeserialize(Message Message)
        {
            _value = Message.ReadLong();
        }
        #endregion
    }
    public class SyncedULong : SyncedType<ulong>
    {
        #region Constructors
        public SyncedULong(SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(0, Owner, SynchronizationType) { }
        public SyncedULong(ulong Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType) { }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            Message.WriteULong(_value);
        }
        protected override void OnDeserialize(Message Message)
        {
            _value = Message.ReadULong();
        }
        #endregion
    }
    public class SyncedFloat : SyncedType<float>
    {
        #region Constructors
        public SyncedFloat(SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(0, Owner, SynchronizationType) { }
        public SyncedFloat(float Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType) { }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            Message.WriteFloat(_value);
        }
        protected override void OnDeserialize(Message Message)
        {
            _value = Message.ReadFloat();
        }
        #endregion
    }
    public class SyncedDouble : SyncedType<double>
    {
        #region Constructors
        public SyncedDouble(SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(0, Owner, SynchronizationType) { }
        public SyncedDouble(double Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType) { }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            Message.WriteDouble(_value);
        }
        protected override void OnDeserialize(Message Message)
        {
            _value = Message.ReadDouble();
        }
        #endregion
    }
    public class SyncedChar : SyncedType<char>
    {
        #region Constructors
        public SyncedChar(SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(' ', Owner, SynchronizationType) { }
        public SyncedChar(char Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType) { }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            Message.WriteChar(_value);
        }
        protected override void OnDeserialize(Message Message)
        {
            _value = Message.ReadChar();
        }
        #endregion
    }
    public class SyncedString : SyncedType<string>
    {
        #region Constructors
        public SyncedString(SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base("", Owner, SynchronizationType) { }
        public SyncedString(string Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType) { }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            Message.WriteString(_value);
        }
        protected override void OnDeserialize(Message Message)
        {
            _value = Message.ReadString();
        }
        #endregion
    }
    public class SyncedSerializable<T> : SyncedType<T>
    {
        #region Constructors
        public SyncedSerializable(T Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType) { }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream memstr = new MemoryStream();
            bf.Serialize(memstr, Value);
            byte[] bytes = memstr.ToArray();
            Message.WriteInt(bytes.Length);
            Message.WriteBytes(bytes);
        }
        protected override void OnDeserialize(Message Message)
        {
            int length = Message.ReadInt();
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream memstr = new MemoryStream(Message.ReadBytes(length));
            _value = (T)bf.Deserialize(memstr);
        }
        #endregion
    }
    public class SyncedNotifySerializable<T> : SyncedType<T> where T : NotifyChangedBase
    {
        #region Constructors
        public SyncedNotifySerializable(T Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType)
        { Value.NotifyChanged += OnNotifyChanged; }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream memstr = new MemoryStream();
            bf.Serialize(memstr, Value);
            byte[] bytes = memstr.ToArray();
            Message.WriteInt(bytes.Length);
            Message.WriteBytes(bytes);
        }
        protected override void OnDeserialize(Message Message)
        {
            int length = Message.ReadInt();
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream memstr = new MemoryStream(Message.ReadBytes(length));
            _value= (T)bf.Deserialize(memstr);
        }
        #endregion
    }
    public class SyncedVector2f : SyncedType<Vector2f>
    {
        #region Constructors
        public SyncedVector2f(SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(new Vector2f(), Owner, SynchronizationType) { }
        public SyncedVector2f(Vector2f Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType) { }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            Message.WriteFloat(_value.X);
            Message.WriteFloat(_value.Y);
        }
        protected override void OnDeserialize(Message Message)
        {
            _value.X = Message.ReadFloat();
            _value.Y = Message.ReadFloat();
        }
        #endregion
    }
    public class SyncedVector2i : SyncedType<Vector2i>
    {
        #region Constructors
        public SyncedVector2i(SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(new Vector2i(), Owner, SynchronizationType) { }
        public SyncedVector2i(Vector2i Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType) { }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            Message.WriteInt(_value.X);
            Message.WriteInt(_value.Y);
        }
        protected override void OnDeserialize(Message Message)
        {
            _value.X = Message.ReadInt();
            _value.Y = Message.ReadInt();
        }
        #endregion
    }
    public class SyncedVector2u : SyncedType<Vector2u>
    {
        #region Constructors
        public SyncedVector2u(SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(new Vector2u(), Owner, SynchronizationType) { }
        public SyncedVector2u(Vector2u Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType) { }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            Message.WriteUInt(_value.X);
            Message.WriteUInt(_value.Y);
        }
        protected override void OnDeserialize(Message Message)
        {
            _value.X = Message.ReadUInt();
            _value.Y = Message.ReadUInt();
        }
        #endregion
    }
    public class SyncedColor : SyncedType<Color>
    {
        #region Constructors
        public SyncedColor(SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Color.Black, Owner, SynchronizationType) { }
        public SyncedColor(Color Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType) { }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            Message.WriteBytes(new byte[] { _value.A, _value.R, _value.G, _value.B });
        }
        protected override void OnDeserialize(Message Message)
        {
            byte[] colorbytes = Message.ReadBytes(4);
            _value.A = colorbytes[0];
            _value.R = colorbytes[1];
            _value.G = colorbytes[2];
            _value.B = colorbytes[3];
        }
        #endregion
    }
    public class SyncedFloatRect : SyncedType<FloatRect>
    {
        #region Constructors
        public SyncedFloatRect(SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(new FloatRect(), Owner, SynchronizationType) { }
        public SyncedFloatRect(FloatRect Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType) { }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            Message.WriteFloat(_value.Left);
            Message.WriteFloat(_value.Top);
            Message.WriteFloat(_value.Width);
            Message.WriteFloat(_value.Height);
        }
        protected override void OnDeserialize(Message Message)
        {
            _value.Left = Message.ReadFloat();
            _value.Top = Message.ReadFloat();
            _value.Width = Message.ReadFloat();
            _value.Height = Message.ReadFloat();
        }
        #endregion
    }
    public class SyncedIntRect : SyncedType<IntRect>
    {
        #region Constructors
        public SyncedIntRect(SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(new IntRect(), Owner, SynchronizationType) { }
        public SyncedIntRect(IntRect Value, SyncedObject Owner, SynchronizationType SynchronizationType = Networking.SynchronizationType.Static)
            : base(Value, Owner, SynchronizationType) { }
        #endregion

        #region Functions
        protected override void OnSerialize(Message Message)
        {
            Message.WriteInt(_value.Left);
            Message.WriteInt(_value.Top);
            Message.WriteInt(_value.Width);
            Message.WriteInt(_value.Height);
        }
        protected override void OnDeserialize(Message Message)
        {
            _value.Left = Message.ReadInt();
            _value.Top = Message.ReadInt();
            _value.Width = Message.ReadInt();
            _value.Height = Message.ReadInt();
        }
        #endregion
    }
}
