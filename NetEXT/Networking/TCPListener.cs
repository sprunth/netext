﻿using System;
using System.Net;
using System.Net.Sockets;
using NetEXT.Concurrent;

namespace NetEXT.Networking
{
    public class TCPListener
    {
        #region Variables
        private TcpListener _listener = null;
        private bool _running = false;
        private Dispatcher _dispatcher = null;
        #endregion

        #region Constructors
        public TCPListener(int Port, Dispatcher CurrentDispatcher = null) : this(Port, IPAddress.Any, CurrentDispatcher) { }
        public TCPListener(int Port, IPAddress Address, Dispatcher CurrentDispatcher = null) : this(new IPEndPoint(Address, Port), CurrentDispatcher) { }
        public TCPListener(IPEndPoint Endpoint, Dispatcher CurrentDispatcher = null)
        {
            _listener = new TcpListener(Endpoint);
            if (CurrentDispatcher == null) _dispatcher = Dispatcher.CurrentDispatcher;
            else _dispatcher = CurrentDispatcher;
        }
        #endregion

        #region Events
        public event Action<TCPListener, TCPSocket> ClientConnected;
        #endregion

        #region Functions
        public bool Start()
        {
            if (!_running)
            {
                try
                {
                    _listener.Start();
                    _listener.BeginAcceptTcpClient(EndAcceptClient, null);
                    _running = true;
                }
                catch
                {
                    _running = false;
                }
            }
            return _running;
        }
        private void EndAcceptClient(IAsyncResult Result)
        {
            if (_running)
            {
                TcpClient joinedclient = _listener.EndAcceptTcpClient(Result);
                if (ClientConnected != null) { _dispatcher.InvokeAsync(ClientConnected, new object[] { this, new TCPSocket(joinedclient, _dispatcher) }); }
                _listener.BeginAcceptTcpClient(EndAcceptClient, null);
            }
        }
        public void Stop()
        {
            if (_running)
            {
                _running = false;
                _listener.Stop();
            }
        }
        #endregion
    }
}
