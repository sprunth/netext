﻿using System;
using SFML.System;

namespace NetEXT.Particles
{
    /// <summary>Interface on which custom particle affectors can be built</summary>
    public interface IAffector
    {
        /// <summary>Called when the affector needs to be applied to a particle</summary>
        /// <param name="Particle">Particle currently being affected</param>
        /// <param name="DeltaTime">Time interval during which particles are affected</param>
        void ApplyAffector(Particle Particle, Time DeltaTime);
    }
}
