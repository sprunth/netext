﻿using System;
using NetEXT.TimeFunctions;
using SFML.Window;
using SFML.Graphics;
using SFML.System;

namespace NetEXT.Particles
{
    /// <summary>This class represents a single particle of a particle system. It stores properties like position, rotation, scale, movement, color and life time.</summary>
    public class Particle
    {
        #region Variables
        private Vector2f _position = new Vector2f(0, 0);
        private Vector2f _velocity = new Vector2f(0, 0);
        private float _rotation = 0;
        private float _rotationspeed = 0;
        private Vector2f _scale = new Vector2f(1, 1);
        private Color _color = Color.White;
        private Time _elapsedlifetime = Time.Zero;
        private Time _totallifetime = Time.Zero;
        private int _textureindex = 0;
        #endregion

        #region Properties
        /// <summary>Gets or sets the position of this particle.</summary>
        public Vector2f Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }
        /// <summary>Gets or sets the velocity of this particle.</summary>
        public Vector2f Velocity
        {
            get
            {
                return _velocity;
            }
            set
            {
                _velocity = value;
            }
        }
        /// <summary>Gets or sets the rotation of this particle.</summary>
        public float Rotation
        {
            get
            {
                return _rotation;
            }
            set
            {
                _rotation = value;
            }
        }
        /// <summary>Gets or sets the rotation speed of this particle.</summary>
        public float RotationSpeed
        {
            get
            {
                return _rotationspeed;
            }
            set
            {
                _rotationspeed = value;
            }
        }
        /// <summary>Gets or sets the scale of this particle.</summary>
        public Vector2f Scale
        {
            get
            {
                return _scale;
            }
            set
            {
                _scale = value;
            }
        }
        /// <summary>Gets or sets the color of this particle.</summary>
        public Color Color
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
            }
        }
        /// <summary>Gets the time since this particle was emitted.</summary>
        public Time ElapsedLifetime
        {
            get
            {
                return _elapsedlifetime;
            }
            internal set
            {
                _elapsedlifetime = value;
            }
        }
        /// <summary>Gets the total time the particle will live.</summary>
        public Time TotalLifetime
        {
            get
            {
                return _totallifetime;
            }
        }
        /// <summary>Gets or sets the texture rect index of the particle.</summary>
        public int TextureIndex
        {
            get
            {
                return _textureindex;
            }
            set
            {
                _textureindex = value;
            }
        }
        /// <summary>Gets the time left for this particle before it dies.</summary>
        public Time RemainingLifetime
        {
            get
            {
                return _totallifetime - _elapsedlifetime;
            }
        }
        /// <summary>Gets the elapsed lifetime / total lifetime.</summary>
        public float ElapsedRatio
        {
            get
            {
                return (float)(ElapsedLifetime.AsSeconds() / TotalLifetime.AsSeconds());
            }
        }
        /// <summary>Gets the remaining lifetime / total lifetime.</summary>
        public float RemainingRatio
        {
            get
            {
                return (float)(RemainingLifetime.AsSeconds() / TotalLifetime.AsSeconds());
            }
        }
        #endregion

        #region Constructors/Destructors
        /// <summary>Constructs a particle with the specified lifetime.</summary>
        /// <param name="TotalLifetime">Time before the particle dies</param>
        public Particle(Time TotalLifetime)
        {
            _totallifetime = TotalLifetime;
        }
        #endregion
    }
}