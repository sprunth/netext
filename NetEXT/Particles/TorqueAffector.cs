﻿using System;
using SFML.Window;
using SFML.System;

namespace NetEXT.Particles
{
    /// <summary>Applies a rotational acceleration to particles over time.</summary>
    public class TorqueAffector : IAffector
    {
        #region Variables
        private float _angularacceleration = 0;
        #endregion

        #region Properties
        /// <summary>Gets or sets the angular acceleration applied to the particles (in degrees).</summary>
        private float AngularAcceleration
        {
            get
            {
                return _angularacceleration;
            }
            set
            {
                _angularacceleration = value;
            }
        }
        #endregion

        #region Constructors/Destructors
        /// <summary>Constructs a new torque affector with the specified angular acceleration.</summary>
        /// <param name="AngularAcceleration">Angular acceleration that will be applied to particles over time</param>
        public TorqueAffector(float AngularAcceleration)
        {
            _angularacceleration = AngularAcceleration;
        }
        #endregion

        #region Functions
        public void ApplyAffector(Particle CurrentParticle, Time ElapsedTime)
        {
            CurrentParticle.RotationSpeed += (float)(ElapsedTime.AsSeconds() * _angularacceleration);
        }
        #endregion
    }
}
