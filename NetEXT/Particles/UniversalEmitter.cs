﻿using System;
using NetEXT.MathFunctions;
using SFML.Window;
using SFML.Graphics;
using SFML.System;

namespace NetEXT.Particles
{
    /// <summary>Class that emits particles with customizable initial conditions.</summary>
    /// <remarks>This emitter is universal with respect to the initial conditions of each emitted particle. It works with callbacks that return initial values for the particle attributes (position, rotation, color, ...). So you can pass constants, random distributions, or any functions that compute the value in an arbitrary way.</remarks>
    public class UniversalEmitter : EmitterBase
    {
        #region Variables
        private float _emissionrate = 1;
        private float _emissiondifference = 0;
        private Distribution<Vector2f> _particleposition = new Vector2f(0, 0);
        private Distribution<Vector2f> _particlevelocity = new Vector2f(0, 0);
        private Distribution<float> _particlerotation = 0;
        private Distribution<float> _particlerotationspeed = 0;
        private Distribution<Vector2f> _particlescale = new Vector2f(1, 1);
        private Distribution<Color> _particlecolor = Color.White;
        private Distribution<Time> _particlelifetime = Time.Zero;
        private Distribution<int> _particletextureindex = 0;
        #endregion

        #region Properties
        /// <summary>Gets or sets the particle emission rate per second.</summary>
        public float EmissionRate
        {
            get
            {
                return _emissionrate;
            }
            set
            {
                _emissionrate = value;
            }
        }
        /// <summary>Sets the position for new particles.</summary>
        public Distribution<Vector2f> ParticlePosition
        {
            set
            {
                _particleposition = value;
            }
        }
        /// <summary>Sets the velocity for new particles.</summary>
        public Distribution<Vector2f> ParticleVelocity
        {
            set
            {
                _particlevelocity = value;
            }
        }
        /// <summary>Sets the rotation for new particles.</summary>
        public Distribution<float> ParticleRotation
        {
            set
            {
                _particlerotation = value;
            }
        }
        /// <summary>Sets the rotation speed for new particles.</summary>
        public Distribution<float> ParticleRotationSpeed
        {
            set
            {
                _particlerotationspeed = value;
            }
        }
        /// <summary>Sets the scale for new particles.</summary>
        public Distribution<Vector2f> ParticleScale
        {
            set
            {
                _particlescale = value;
            }
        }
        /// <summary>Sets the color for new particles.</summary>
        public Distribution<Color> ParticleColor
        {
            set
            {
                _particlecolor = value;
            }
        }
        /// <summary>Sets the lifetime for new particles.</summary>
        public Distribution<Time> ParticleLifetime
        {
            set
            {
                _particlelifetime = value;
            }
        }
        /// <summary>Sets the texture index for new particles.</summary>
        public Distribution<int> ParticleTextureIndex
        {
            set
            {
                _particletextureindex = value;
            }
        }
        #endregion

        #region Functions
        public override void EmitParticles(ParticleSystem ParticleSystem, Time DeltaTime)
        {
            int numberofparticles = CalculateParticleCount(DeltaTime);
            for (int i = 1; i <= numberofparticles; i++)
            {
                Particle newparticle = new Particle(_particlelifetime) { Position = _particleposition, Velocity = _particlevelocity, Rotation = _particlerotation, RotationSpeed = _particlerotationspeed, Scale = _particlescale, Color = _particlecolor, TextureIndex = _particletextureindex };
                base.EmitParticle(ParticleSystem, newparticle);
            }
        }
        private int CalculateParticleCount(Time DeltaTime)
        {
            float particleamount = (float)(_emissionrate * DeltaTime.AsSeconds()) + _emissiondifference;
            int numberofparticles = (int)Math.Floor(particleamount);
            _emissiondifference = particleamount - (float)numberofparticles;
            return numberofparticles;
        }
        #endregion
    }
}
