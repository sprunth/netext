﻿using System;
using System.Collections.Generic;
using NetEXT.Utility;

namespace NetEXT.Resources
{
    /// <summary>Class that is responsible for the management of resources like images, fonts or music.</summary>
    /// <typeparam name="T">Type of resource to cache</typeparam>
    /// <typeparam name="U">Type that will be used to identify resources</typeparam>
    /// <remarks>You can acquire and release resources. Access is granted through handles.</remarks>
    public class ResourceCache<T, U> where T : IDisposable
    {
        #region Variables
        private ResourceStrategies _strategy = ResourceStrategies.AutoRelease;
        private Dictionary<U, ResourceHolder<T, U>> _resources = new Dictionary<U, ResourceHolder<T, U>>();
        private DynamicDelegate _constructiondelegate = null;
        #endregion

        #region Properties
        /// <summary>Gets or sets the strategy for managing resource lifetimes.</summary>
        /// <remarks>The strategy will only be applied for each resource that will be acquired in the future. Resources that have already been allocated are not concerned! This allows you to set the release behaviour specificially for each resource before you aquire new resources.</remarks>
        public ResourceStrategies ResourceStrategy
        {
            get
            {
                return _strategy;
            }
            set
            {
                _strategy = value;
            }
        }
        /// <summary>Gets or sets the callbacks that are used to construct new resources.</summary>
        public DynamicDelegate ConstructionDelegate
        {
            get
            {
                return _constructiondelegate;
            }
            set
            {
                _constructiondelegate = value;
            }
        }
        #endregion

        #region Contructors
        /// <summary>Constructs a resource cache with the specified constructor callbacks and strategy.</summary>
        /// <param name="ConstructionDelegate">Callbacks that will construct new resources</param>
        /// <param name="ResourceStrategy">Strategy that will determine how resources are disposed</param>
        public ResourceCache(DynamicDelegate ConstructionDelegate, ResourceStrategies ResourceStrategy = ResourceStrategies.AutoRelease)
        {
            _strategy = ResourceStrategy;
            _constructiondelegate = ConstructionDelegate;
        }
        #endregion

        #region Functions
        /// <summary>Aquires an already loaded resource.</summary>
        /// <param name="Key">Key of the resource</param>
        /// <returns>Handle to the aquired resource.</returns>
        /// <remarks>If the resource has not been loaded then it will attempt to be constructed with an empty constructor.</remarks>
        public ResourceHandle<T, U> Aquire(U Key) { return Aquire(Key, new object[0]); }
        /// <summary>Aquires or loads a resource.</summary>
        /// <param name="Key">Key of the resource</param>
        /// <param name="Parameter">Single parameter that will be passed to construct the resource</param>
        /// <returns>Handle to the aquired resource.</returns>
        /// <remarks>If the resource has not been loaded then it will attempt to be constructed with a single parameter constructor.</remarks>
        public ResourceHandle<T, U> Aquire(U Key, object Parameter) { return Aquire(Key, new object[] { Parameter }); }
        /// <summary>Aquires or loads a resource.</summary>
        /// <param name="Key">Key of the resource</param>
        /// <param name="Parameters">Array of parameters that will be passed to construct the resource</param>
        /// <returns>Handle to the aquired resource.</returns>
        /// <remarks>If the resource has not been loaded then it will attempt to be constructed with a constructor that matches the parameter array.</remarks>
        public ResourceHandle<T, U> Aquire(U Key, object[] Parameters)
        {
            if (_resources.ContainsKey(Key)) return new ResourceHandle<T, U>(_resources[Key]);
            else
            {
                ResourceHolder<T, U> newresource = new ResourceHolder<T, U>(ConstructionDelegate.InvokeDelegate<T>(Parameters), _strategy);
                _resources.Add(Key, newresource);
                newresource.ResourceNotReferenced += ResourceNotReferenced;
                newresource.Key = Key;
                return new ResourceHandle<T, U>(newresource);
            }
        }
        /// <summary>Explicitly releases a resource.</summary>
        /// <param name="Key">Key of the resource</param>
        /// <remarks>If a resource is currently in use then it will be released as soon as all references to it are lost.</remarks>
        public void Release(U Key)
        {
            if (!_resources.ContainsKey(Key)) return;
            _resources[Key].DisposeResource();
            _resources[Key].ResourceNotReferenced -= ResourceNotReferenced;
            _resources.Remove(Key);
        }
        private void ResourceNotReferenced(ResourceHolder<T, U> Resource, ResourceStrategies Strategy)
        {
            if (Strategy == ResourceStrategies.AutoRelease)
            {
                Release(Resource.Key);
            }
        }
        #endregion
    }
}
