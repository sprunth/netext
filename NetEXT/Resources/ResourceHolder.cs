﻿using System;

namespace NetEXT.Resources
{
    internal class ResourceHolder<T, U> where T : IDisposable
    {
        #region Variables
        private int _referencecount = 0;
        private bool _shoulddispose = false;
        private bool _isdisposed = false;
        private U _key = default(U);
        private T _resource = default(T);
        private ResourceStrategies _strategy = ResourceStrategies.AutoRelease;
        #endregion

        #region Properties
        internal int ReferenceCount
        {
            get
            {
                return _referencecount;
            }
            set
            {
                _referencecount = value;
                if (ReferenceCount == 0 && _shoulddispose) DisposeResource();
                if (ReferenceCount == 0 && ResourceNotReferenced != null) ResourceNotReferenced(this, _strategy);
            }
        }
        internal U Key
        {
            get
            {
                return _key;
            }
            set
            {
                _key = value;
            }
        }
        internal T InternalResource
        {
            get
            {
                return _resource;
            }
        }
        #endregion

        #region Events
        internal event Action<ResourceHolder<T, U>, ResourceStrategies> ResourceNotReferenced;
        #endregion

        #region Constructors
        public ResourceHolder(T Resource, ResourceStrategies Strategy)
        {
            _resource = Resource;
            _strategy = Strategy;
        }
        #endregion

        #region Functions
        internal void DisposeResource()
        {
            if (_isdisposed) return;
            if (ReferenceCount == 0)
            {
                _isdisposed = true;
                _resource.Dispose();
            }
            else
            {
                _shoulddispose = true;
            }
        }
        #endregion
    }
}
