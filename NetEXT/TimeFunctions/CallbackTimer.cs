﻿using System;
using SFML.System;
using NetEXT.Input;
using NetEXT.Utility;

namespace NetEXT.TimeFunctions
{
    /// <summary>Advanced timer with the ability to trigger function calls.</summary>
    /// <remarks>Clock class that counts time down. As an extension of Timer, this class is able to register functions that are called at expiration time.</remarks>
    public class CallbackTimer : Timer
    {
        #region Variables
        private CallbackSequence<Action<CallbackTimer>> _callbacks = new CallbackSequence<Action<CallbackTimer>>();
        private bool _justexpired = true;
        #endregion

        #region Constructors
        /// <summary>Creates a callback timer that is initially expired.</summary>
        public CallbackTimer() { }
        #endregion

        #region Functions
        /// <summary>Resets the timer's remaining time to the given limit and stops it.</summary>
        /// <param name="TimeLimit">Time limit to wind the timer with</param>
        /// <remarks>In contrast to Restart(), the timer is not running after the call.</remarks>
        public override void Reset(Time TimeLimit)
        {
            base.Reset(TimeLimit);
            _justexpired = false;
        }
        /// <summary>Resets the timer's remaining time to the given limit and starts it again.</summary>
        /// <param name="TimeLimit">Time limit to wind the timer with</param>
        public override void Restart(Time TimeLimit)
        {
            base.Restart(TimeLimit);
            _justexpired = false;
        }
        /// <summary>This is the most important function of this class. You should call this every frame.</summary>
        public void Update()
        {
            if (base.IsExpired && !_justexpired)
            {
                _justexpired = true;
                _callbacks.InvokeCallbacks(this);
            }
        }
        /// <summary>Registers a function which will be called when the time reaches zero.</summary>
        /// <param name="Listener">Function you want to associate with the timer expiration.</param>
        public void Connect(Action<CallbackTimer> Callback)
        {
            _callbacks.RegisterCallback(Callback);
        }
        /// <summary>Removes all currently associated timer listeners.</summary>
        public void ClearConnections()
        {
            _callbacks.ClearCallbacks();
        }
        #endregion
    }
}
