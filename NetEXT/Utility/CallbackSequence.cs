﻿using System;
using System.Collections.Generic;

namespace NetEXT.Utility
{
    /// <summary>Class that is similar to the <see cref="System.Delegate"/> class, however this intends to be easier to use.</summary>
    /// <remarks>You can register and remove callbacks with two very simple functions. It is also possible to easily view the list of callbacks with the Callbacks property.</remarks>
    /// <typeparam name="T">Type of delegate that will be used for callbacks - note this must be a subclass of a <see cref="System.Delegate"/></typeparam>
    public class CallbackSequence<T>
    {
        #region Variables
        private List<T> _callbacks = new List<T>();
        #endregion

        #region Properties
        /// <summary>Gets a list of all registered callbacks</summary>
        public T[] Callbacks
        {
            get
            {
                return _callbacks.ToArray();
            }
        }
        #endregion

        #region Constructors
        static CallbackSequence()
        {
            if (!typeof(T).IsSubclassOf(typeof(Delegate)))
            {
                throw new InvalidOperationException(typeof(T).Name + " is not a delegate type");
            }
        }
        #endregion

        #region Functions
        /// <summary>Registers a callback to be invoked.</summary>
        /// <param name="Callback">Callback that will be registered</param>
        public void RegisterCallback(T Callback)
        {
            _callbacks.Add(Callback);
        }
        /// <summary>Removes a callback.</summary>
        /// <param name="Callback">Callback to be removed</param>
        public void UnregisterCallback(T Callback)
        {
            _callbacks.Remove(Callback);
        }
        /// <summary>Removes all currently registered delegates.</summary>
        public void ClearCallbacks()
        {
            _callbacks.Clear();
        }
        /// <summary>Invokes all callbacks without passing any parameters.</summary>
        /// <returns>Result of the last callback.</returns>
        public object InvokeCallbacks() { return InvokeCallbacks(new object[0]); }
        /// <summary>Invokes all callbacks and passes a single parameter.</summary>
        /// <returns>Result of the last callback.</returns>
        public object InvokeCallbacks(object Parameter) { return InvokeCallbacks(new object[] { Parameter }); }
        /// <summary>Invokes all callbacks and passes the parameter array.</summary>
        /// <returns>Result of the last callback.</returns>
        public object InvokeCallbacks(object[] Parameters)
        {
            for (int i = 0; i < _callbacks.Count; i++)
            {
                dynamic call = _callbacks[i];
                if (i == _callbacks.Count - 1) return ((Delegate)call).DynamicInvoke(Parameters);
                else ((Delegate)call).DynamicInvoke(Parameters);
            }
            return null;
        }
        #endregion

        #region Operators
        public static CallbackSequence<T> operator +(CallbackSequence<T> LHS, CallbackSequence<T> RHS)
        {
            CallbackSequence<T> combined = new CallbackSequence<T>();
            foreach (var call in LHS.Callbacks) { combined.RegisterCallback(call); }
            foreach (var call in RHS.Callbacks) { combined.RegisterCallback(call); }
            return combined;
        }
        #endregion
    }
}
